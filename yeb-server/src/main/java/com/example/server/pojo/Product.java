package com.example.server.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 商品表
 * </p>
 *
 * @author 小红
 * @since 2022-12-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("t_product")
@ApiModel(value="Product对象", description="商品表")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "商品ID")
    @TableId(value = "product_id", type = IdType.AUTO)
    @JsonProperty("product_id")
    private Integer productId;

    @ApiModelProperty(value = "商品图片")
    @TableField("product_img_url")
    @JsonProperty("product_img_url")
    private String productImgUrl;

    @ApiModelProperty(value = "商品价格")
    @TableField("product_price")
    @JsonProperty("product_price")
    private Double productPrice;

    @ApiModelProperty(value = "商品名称")
    @TableField("product_name")
    @JsonProperty("product_name")
    private String productName;

    @ApiModelProperty(value = "商品折扣价")
    @TableField("product_uprice")
    @JsonProperty("product_uprice")
    private Double productUprice;


}
