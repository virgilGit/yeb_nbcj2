package com.example.server.pojo;

import lombok.Data;

@Data
public class Customer {

    private String firstName;

    private String lastName;
}
