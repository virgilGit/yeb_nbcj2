package com.example.server.utils;

import org.apache.tomcat.util.codec.binary.Base64;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class RSAUtils {

    public static String priKey = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDELnalSTpsJab3\n" +
            "RxVLQVWji9uuZgfEo5/S2O5zoP0Hw9JvD+onN3+9mWR94ZHY/W+0U7MnFgTwrnHy\n" +
            "LTnKaGoHSCQz1jivwll0L5UY63F7w1I3qPS9w1KlS0MG6OlpS1xj7/KAPv5exb1V\n" +
            "wy1WdhKcIauk6ZrW8lCs6GSYO8YVW4SzT4w+124mzEqUhU6DaYLtfyQNPH//kARF\n" +
            "m0ITi1ftXjnfdbphSMhFb+tPgbV7y4CkCyG2KbOeY9zN1YvA361J4iLNOqbM4Yn1\n" +
            "9N1Pt6VQeKlwRkZm1YXYYHKubi92SbBz+0OKHqzymAO+gJj9hWK2KxuoNuuOrq3P\n" +
            "T4Z7iZxrAgMBAAECggEAPNhESAMsMXqEDFFpk0uSKgUofzB53Ez7NsO9QPN/Rrc/\n" +
            "Pt7qunsGDWHTvw8E0w3XZVBIUikMIzGXeYr5T4OOzu9a+xDHyAs4/0X6sh3jtoTf\n" +
            "rQ+jyG8Z5NYw1aXLLrMG1qAG0Cu5bN9bV4wPNa+tGet/dshVE7FXwCjbejOfveN2\n" +
            "XyJzfSe4lQLUq73rlekDWffKCBTtyacxMjVxxYldomb/aDZXFimfickWWcM545kR\n" +
            "Q3vqldNGIGa1lCkZDi9Fl82zzivKDkFc6ej7i1VZ9O3gzv1PvCs6WwVLBBNcPPEj\n" +
            "9US4LN1JsKc4S1YNXvPL3G1EMHWdMqV/zeITBmdFcQKBgQD+vugoRDF0/RBFyWgI\n" +
            "5De+kDcEvhl3Xq5jQRXgKDFmQimra8Z9lWApeeO2x/0731opfq1TXnt2wsorjA6i\n" +
            "Z+r5YBPCbJBg1xoxRiExO1WIKTEx5SPvz4YyI1BsYDoMqhMC39uvKJu9K6DifBAP\n" +
            "fr9Hit5T0gVstL6UYqXn83y2PQKBgQDFJb1UNynK9NgVb7g+5f6KhGLy88ND5E0T\n" +
            "iUlOVbY/m5R0d2P6+GJdT2GVPvowhreojRtUjhD7F6QyV3tT9Ub5ZbjE1aZdto+D\n" +
            "EIfCF1gl1xp08CRlB/hQ7u1WngYvvcM/sn5GDAzxQ53FaXQhWXqT+Ylg6oyCWXAb\n" +
            "MgxgIZnvxwKBgQDrEMo07cgdRa8QVmszWTO/GmBKdggVyD5uxA4li7EZxQ+iaI7+\n" +
            "LypO23SuoxgfeVg/Ve8mMNcInXHkgRa38eA3QNq622bWlWZL9YSHY1Lnk+sbOW/R\n" +
            "x9LyzPdXKewm54Enc93JYVVkV6/bjdNcGP70S0YgXno29wms7uJciKeZmQKBgQC5\n" +
            "++pqgkfZAbbAMk78d3j0MAQw8qmpaL0VyxPu5Ikz5I/ed6tIXf5dzL9gTrM0CENG\n" +
            "rSe1UvjOB/WM0F62S9QS/1WZy3xROCKXIIcSIXe06wI4gzuyXBx0LZKVfiqT609A\n" +
            "ziC18DRdfs5eKCe4NnOfGW/ykBLR/ZH29fYa0lgojQKBgQCk7TDoJtbS1pPtgaZO\n" +
            "jVYxT/oyNeHXZ8g4y7qjo8Rj4+JpXjls20n3KizfnvUE7N1DLP2nnm4gfaaJYWrO\n" +
            "OdQPDxOvN9Ap3DjaL22Vlq9uqlTLGxmeVlQR4BiwBe4jzqXdBlryIwqDsKa238/9\n" +
            "nPsBpn4TmDM1FM5uks75vA3y6g==";

    public static String pubKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxC52pUk6bCWm90cVS0FV\n" +
            "o4vbrmYHxKOf0tjuc6D9B8PSbw/qJzd/vZlkfeGR2P1vtFOzJxYE8K5x8i05ymhq\n" +
            "B0gkM9Y4r8JZdC+VGOtxe8NSN6j0vcNSpUtDBujpaUtcY+/ygD7+XsW9VcMtVnYS\n" +
            "nCGrpOma1vJQrOhkmDvGFVuEs0+MPtduJsxKlIVOg2mC7X8kDTx//5AERZtCE4tX\n" +
            "7V4533W6YUjIRW/rT4G1e8uApAshtimznmPczdWLwN+tSeIizTqmzOGJ9fTdT7el\n" +
            "UHipcEZGZtWF2GByrm4vdkmwc/tDih6s8pgDvoCY/YVitisbqDbrjq6tz0+Ge4mc\n" +
            "awIDAQAB";

    public static String sign(String plainText) throws Throwable {

        PrivateKey privateKey =
                KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(Base64.decodeBase64(priKey)));

        Signature signature = Signature.getInstance("NONEwithRSA");

        // 将使用 私钥 进行 签名
        signature.initSign(privateKey);

        // 将数据原文 -> 私钥 -> 加密 -> 密文（签名）
        signature.update(plainText.getBytes());

        byte[] sign = signature.sign();

        return Base64.encodeBase64String(sign);
    }

    public static boolean verify(String plainText, String sign) throws Throwable {

        PublicKey publicKey = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decodeBase64(pubKey)));

        Signature signature = Signature.getInstance("NONEwithRSA");

        signature.initVerify(publicKey);

        signature.update(plainText.getBytes());

        boolean verify = signature.verify(Base64.decodeBase64(sign));

        return verify;
    }

    public static void main(String[] args) throws Throwable {

        String sign = sign("{\"firstName\":\"virgil\",\"lastName\":\"Stark\"}");
        System.out.println(sign);

        boolean success = verify("{\"firstName\":\"virgil\",\"lastName\":\"Stark\"} ", sign);
        System.out.println(success);
    }
}
