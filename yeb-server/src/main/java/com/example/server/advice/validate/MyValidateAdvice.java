package com.example.server.advice.validate;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

@Aspect
@Component
public class MyValidateAdvice {

    @Pointcut("@annotation(com.example.server.advice.validate.MyValidated)")
    public void validatePointcut() {

    }

    @Before("validatePointcut()")
    public void validateAdvice(JoinPoint joinPoint) {

        /**
         * 将所有的参数args拿出来 参数的定义、具体参数的值
         * 遍历这些参数，判断参数是否被注解MyRequired修饰
         * 是的话，验证数据是否符合规则
         */
        Object[] args = joinPoint.getArgs();
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        // 获取到该方法的所有参数
        Parameter[] parameters = method.getParameters();

        // 报错收集器
        StringBuilder stringBuilder = new StringBuilder();
        boolean flag = true;

        for (int i = 0; i < parameters.length; i++) {

            boolean myRequired = parameters[i].isAnnotationPresent(MyRequired.class);

            if (myRequired) {

                // 判断该参数是否为空
                if (args[i] == null) {

                    flag = false;
                    stringBuilder.append(parameters[i].getName() + "不能为空对象！\n");
                } else {

                    if (args[i] instanceof String && "".equals(args[i])) {
                        flag = false;
                        stringBuilder.append(parameters[i].getName() + "不能为空字符串！\n");
                    }
                }
                // String的判断，不仅判断是空对象、判断空字符串 "" --> Mybatis <if test="">
            }
        }

        if (!flag) {
            throw new RuntimeException(stringBuilder.toString());
        }
    }
}
