package com.example.server.advice.validate;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface MyRequired {
    /**
     * 修饰方法的参数，被修饰的参数需要按照规则进行数据校验
     */
}
