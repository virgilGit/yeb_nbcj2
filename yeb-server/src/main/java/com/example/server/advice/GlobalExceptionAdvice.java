package com.example.server.advice;

import com.example.server.security.YebException;
import com.example.server.vo.RespBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.condition.RequestConditionHolder;

import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
public class GlobalExceptionAdvice {

    private final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionAdvice.class);

    @ExceptionHandler(ArithmeticException.class)
    public RespBean arithmeticExceptionHandler(ArithmeticException exception) {

        // 打印日志
        LOGGER.error("算术异常：" + exception.getMessage() , exception);
        // 翻译翻译，发送给客户端
        return RespBean.error(101, "算术异常：" + exception.getMessage());
    }

    @ExceptionHandler(RuntimeException.class)
    public RespBean runtimeException(RuntimeException runtimeException) {

        LOGGER.error("未知的运行时异常：" + runtimeException.getMessage());

        return RespBean.error(500, "未知的运行时异常：" + runtimeException.getMessage());
    }

    @ExceptionHandler(YebException.class)
    public RespBean yebExceptionHandler(YebException yebException) {

        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        requestAttributes.getResponse().setStatus(HttpServletResponse.SC_OK);

        LOGGER.error(yebException.getMessage());

        return RespBean.error(yebException.getCode(), yebException.getMessage());
    }
}
