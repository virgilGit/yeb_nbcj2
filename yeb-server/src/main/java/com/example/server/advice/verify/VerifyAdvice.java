package com.example.server.advice.verify;

import com.example.server.utils.RSAUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Component
@Aspect // 切面：切入点、增强
public class VerifyAdvice {

    @Pointcut("@annotation(com.example.server.advice.verify.MyVerify)")
    public void verifyPointCut() {

    }

    @Before("verifyPointCut()")
    public void verifyBefore(JoinPoint joinPoint) {

        try{
            // 数据原文
            Object[] args = joinPoint.getArgs();
            ObjectMapper objectMapper = new ObjectMapper();
            String json = objectMapper.writeValueAsString(args[0]);

            // 签名
            ServletRequestAttributes requestAttributes =
                    (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            // 用来描述当前请求的对象
            // 每一次请求进来了，经过tomcat，为请求开辟一个线程，该线程之内只有一个request response
            HttpServletRequest request = requestAttributes.getRequest();

            String signature = request.getHeader("signature");

            boolean verify = RSAUtils.verify(json, signature);

            if (!verify) {
                throw new RuntimeException("签名不匹配！");
            }
        } catch (Throwable throwable) {
            // 将逻辑链顺到 GlobalExceptionHandler 全局异常处理器
            throw new RuntimeException("签名验证时异常：" + throwable.getMessage());
        }
    }
}
