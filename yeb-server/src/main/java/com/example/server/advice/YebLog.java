package com.example.server.advice;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME) // 该注解的生命周期，RUNTIME表示代码运行时依旧有效
public @interface YebLog {

    String name() default "未知方法";
}
