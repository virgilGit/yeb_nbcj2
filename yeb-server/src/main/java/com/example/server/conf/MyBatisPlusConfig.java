package com.example.server.conf;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyBatisPlusConfig {

    /**
     * 手动向spring容器中注册一个对象，对象的类型是方法的返回类型（PaginationInterceptor）
     * 对象的id 就是方法名paginationInterceptor
     * 这种写法是为了弥补在使用第三方插件时，没法通过编写类直接注册对象
     *
     * @Controller // 将当前类new一个对象，放到spring容器中
     * public TestController{
     *
     *
     * }
     *
     * interface TestInter ;
     *
     * class TestA
     * class TestB
     *
     * 配置文件中追加一个属性
     *
     * @Bean
     * public TestInter testInter() {
     *
     *     读取配置文件中的配置信息
     *     if(A) return new TestA()
     *      else if(B) retuen new TestB();
     * }
     *
     * @return
     */

    @Bean
    public PaginationInterceptor paginationInterceptor() {

        return new PaginationInterceptor();
    }
}
