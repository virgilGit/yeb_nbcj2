package com.example.server.conf;

import com.example.server.security.BCryptPasswordEncoder;
import com.example.server.security.Md5PasswordEncoder;
import com.example.server.security.PasswordEncoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class YebConfig {

    @Value("${yeb.passwordEncode}")
    public String passwordType;

    @Bean
    public PasswordEncoder passwordEncoder() {

        if ("BCRYPT".equals(passwordType)) {
            return new BCryptPasswordEncoder();
        } else if ("MD5".equals(passwordType)) {
            return new Md5PasswordEncoder();
        }
        return null;
    }
}
