package com.example.server.conf;

import com.example.server.security.interceptor.AuthenticationInterceptor;
import com.example.server.security.interceptor.AuthorizationInterceptor;
import com.example.server.security.interceptor.JwtUserDetailsLoadingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration // 交给spring 管理； 定位： 配置类
public class WebMvcConfig extends WebMvcConfigurationSupport {

    @Value("${yeb.enableAuthenticate}")
    private boolean enableAuthenticate;

    @Value("${yeb.enableAuthorize}")
    private boolean enableAuthorize;

    @Value("${yeb.authenticate}")
    private String authenticateType;


    @Autowired
    private AuthenticationInterceptor authenticationInterceptor;

    @Autowired
    private AuthorizationInterceptor authorizationInterceptor;

    @Autowired
    private JwtUserDetailsLoadingInterceptor jwtUserDetailsLoadingInterceptor;

    @Override
    protected void addInterceptors(InterceptorRegistry registry) { // 添加拦截器

        if (enableAuthenticate) {

            // 加载基于jwt的解析个人信息的拦截器
            if ("JWT".equals(authenticateType)) {
                registry.addInterceptor(jwtUserDetailsLoadingInterceptor).addPathPatterns("/**")
                        .excludePathPatterns("/login")
                        .excludePathPatterns("/doc.html")
                        .excludePathPatterns("/webjars/**")
                        .excludePathPatterns("/asserts/**")
                        .excludePathPatterns("/swagger-resources");
            }

            // 认证、登录拦截器
            registry.addInterceptor(authenticationInterceptor).addPathPatterns("/**")
                    .excludePathPatterns("/login")
                    .excludePathPatterns("/doc.html")
                    .excludePathPatterns("/webjars/**")
                    .excludePathPatterns("/asserts/**")
                    .excludePathPatterns("/swagger-resources");

            // 授权拦截器
            if (enableAuthorize) {
                registry.addInterceptor(authorizationInterceptor).addPathPatterns("/**")
                        .excludePathPatterns("/login")
                        .excludePathPatterns("/doc.html")
                        .excludePathPatterns("/webjars/**")
                        .excludePathPatterns("/asserts/**")
                        .excludePathPatterns("/swagger-resources");
            }
        }
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) { // 添加静态资源

        registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");

        registry.addResourceHandler("/doc.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}