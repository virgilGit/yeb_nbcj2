package com.example.server.conf;

import com.example.server.security.JwtBasedSecurityContextHolder;
import com.example.server.security.SecurityContextHolder;
import com.example.server.security.SessionBasedSecurityContextHolder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SecurityContextHolderConfig {

    @Value("${yeb.authenticate}")
    public String authenticateType;

    @Bean
    public SecurityContextHolder securityContextHolder() {

        if ("JWT".equals(authenticateType)) {
            return new JwtBasedSecurityContextHolder();
        } else if ("SESSION".equals(authenticateType)) {
            return new SessionBasedSecurityContextHolder();
        }

        return new SessionBasedSecurityContextHolder();
    }
}
