package com.example.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class RespBean {

    private Integer code;

    private String message;

    private Object data;

    public static RespBean success(String message, Object data) {

        return new RespBean(200, message, data);
    }

    public static RespBean success(String message) {

        return new RespBean(200, message, null);
    }

    public static RespBean success(Object data) {

        return new RespBean(200, "请求成功", data);
    }

    public static RespBean success() {

        return new RespBean(200, "请求成功", null);
    }

    public static RespBean error(Integer code, String message) {

        return new RespBean(code, message, null);
    }
}
