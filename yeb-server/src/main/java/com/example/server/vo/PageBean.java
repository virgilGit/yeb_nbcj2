package com.example.server.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * <?>
 * @param <T>
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class PageBean<T> {

    private Integer total;

    private Integer totalPage;

    private Integer currentPage;

    private Integer size;

    private List<T> rows;
}
