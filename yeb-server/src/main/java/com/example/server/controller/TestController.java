package com.example.server.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.server.advice.YebLog;
import com.example.server.advice.validate.MyRequired;
import com.example.server.advice.validate.MyValidated;
import com.example.server.advice.verify.MyVerify;
import com.example.server.pojo.Admin;
import com.example.server.pojo.Customer;
import com.example.server.vo.RespBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

@RestController
@Api(tags = "测试控制器")
public class TestController {

    @GetMapping("/test1")
    @ApiOperation("测试1")
    @YebLog(name = "测试1")
    public String test1() {

        return "success!";
    }

    @GetMapping("/test2")
    @ApiOperation("测试2")
    public RespBean test2() {

        int i = 1/0;
        return RespBean.success();
    }

    /**
     * @ReqestParam 读取请求参数，问号后面的数据
     *  HttpServletRequest#getParameter("myName")
     *  读取表单中的数据
     *  如果参数不加注解，该注解就是默认的注解，并且映射的key值和变量名一致
     */
    @GetMapping("/test3")
    public RespBean test3(@RequestParam("myName") String name, @RequestParam("myAge") Integer age) {

        return RespBean.success("name:" + name + " age:" +age);
    }

    @GetMapping("/test4")
    public RespBean test4(String name, Integer age) {

        return RespBean.success("name:" + name + " age:" +age);
    }

    @GetMapping("/test5")
    public RespBean test5(Admin admin) {

        return RespBean.success(admin);
    }

    @PostMapping("/test6")
    public RespBean test6(@RequestBody Admin admin) {

        // json
        // 客户端发送请求 -> json -> @RequestBody -> java 对象
        // RespBean -> @ResponseBody -> json -> 客户端
        // @RestController -> rest @ResponseBody + @Controller
        return RespBean.success(admin);
    }

    @PostMapping("/test7")
    public RespBean test7(@RequestBody JSONObject data) {

        Object username = data.get("username");
        System.out.println(username);

        return RespBean.success(data);
    }

    /**
     * /test8/1
     * /test8/a
     *
     * @param cid
     * @return
     */
    @DeleteMapping("/test8/{cid}")
    public RespBean test8(@PathVariable Integer cid) {

        return RespBean.success("需要删除： " + cid);
    }

    @GetMapping("/test9")
    public RespBean test9(@RequestHeader String token) {

        return RespBean.success(token);
    }

    @PostMapping("/test/verify")
    @MyVerify
    public RespBean verify(@RequestBody Customer customer,
                           @RequestHeader String signature) {

        // 验证签名

        return RespBean.success(customer);
    }

    @GetMapping("/test10")
    @MyValidated
    public RespBean test10(
            @MyRequired @RequestParam(required = false) String name,
            @MyRequired @RequestParam(required = false) Integer age
    ) {

        return RespBean.success(name + ":" + age);
    }
}
