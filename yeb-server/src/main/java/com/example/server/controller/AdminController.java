package com.example.server.controller;


import com.example.server.pojo.Admin;
import com.example.server.service.IAdminService;
import com.example.server.vo.RespBean;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
@RestController
@RequestMapping("/admin")
@Api(tags = "Admin管理器")
public class AdminController {

    @Autowired
    private IAdminService adminService;

    @GetMapping("/")
    @ApiOperation("查询全部")
    public RespBean list() {

        // select* from t_admin
        List<Admin> list = adminService.list();
        return RespBean.success(list);
    }

    @PostMapping("/")
    @ApiOperation("新增admin")
    public RespBean addAdmin(@RequestBody Admin admin) {

        // insert into t_admin(....) values (?, ?, ? ... ...)
        boolean save = adminService.save(admin);

        return save ? RespBean.success() : RespBean.error(301, "新增出错！");
    }

    @PutMapping("/")
    @ApiOperation("修改admin")
    public RespBean editAdmin(@RequestBody Admin admin) {

        boolean update = adminService.updateById(admin);

        return update ? RespBean.success() : RespBean.error(302, "修改失败！");
    }

    @DeleteMapping("/{id}")
    @ApiOperation("根据admin的id删除信息")
    public RespBean removeById(@PathVariable Integer id) {

        boolean remove = adminService.removeById(id);

        return remove ? RespBean.success() : RespBean.error(303, "删除失败！");
    }

    @GetMapping("/list")
    @ApiOperation("多条件查询")
    public RespBean listByConditions(Admin admin) {

        List<Admin> result = adminService.getByConditions(admin);

        return RespBean.success(result);
    }

    @PostMapping("/save/batch")
    public RespBean saveBatch(@RequestBody List<Admin> admins) {

        boolean result = adminService.addBatch(admins);

        return result ? RespBean.success() : RespBean.error(304, "批量新增失败");
    }

}
