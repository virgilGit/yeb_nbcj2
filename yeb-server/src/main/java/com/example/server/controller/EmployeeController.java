package com.example.server.controller;


import com.example.server.pojo.Employee;
import com.example.server.service.IEmployeeService;
import com.example.server.vo.PageBean;
import com.example.server.vo.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private IEmployeeService employeeService;

    @GetMapping("/list/page")
    public RespBean listInPage(
            @RequestParam(defaultValue = "1") Integer currentPage,
            @RequestParam(defaultValue = "5") Integer size
    ) {

        PageBean<Employee> result =  employeeService.getListInPage(currentPage, size);

        return RespBean.success(result);
    }

    @GetMapping("/list/page2")
    public RespBean listInPage2(@RequestParam(defaultValue = "1") Integer currentPage,
                                @RequestParam(defaultValue = "5") Integer size) {

        PageBean<Employee> result = employeeService.getListInPage2(currentPage, size);

        return RespBean.success(result);
    }
}
