package com.example.server.controller;

import com.example.server.pojo.Admin;
import com.example.server.service.IAdminService;
import com.example.server.vo.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class LoginController {

    @Autowired
    private IAdminService adminService;

    @PostMapping("/login")
    public RespBean login(@RequestBody Admin admin, HttpServletRequest request) {

        RespBean result = adminService.login(admin.getUsername(), admin.getPassword(), request);

        return result;
    }
}
