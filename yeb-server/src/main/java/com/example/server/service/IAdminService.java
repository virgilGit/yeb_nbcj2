package com.example.server.service;

import com.example.server.pojo.Admin;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.server.vo.RespBean;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
public interface IAdminService extends IService<Admin> {

    List<Admin> getByConditions(Admin admin);

    boolean addBatch(List<Admin> admins);

    RespBean login(String username, String password, HttpServletRequest request);
}
