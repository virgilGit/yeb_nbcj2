package com.example.server.service.impl;

import com.example.server.pojo.Product;
import com.example.server.mapper.ProductMapper;
import com.example.server.service.IProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author 小红
 * @since 2022-12-03
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

}
