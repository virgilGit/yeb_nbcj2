package com.example.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.server.mapper.MenuMapper;
import com.example.server.pojo.Admin;
import com.example.server.mapper.AdminMapper;
import com.example.server.pojo.Menu;
import com.example.server.security.JwtBasedSecurityContextHolder;
import com.example.server.security.PasswordEncoder;
import com.example.server.security.SecurityContextHolder;
import com.example.server.security.YebException;
import com.example.server.service.IAdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.server.vo.RespBean;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements IAdminService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SecurityContextHolder securityContextHolder;

    @Autowired
    private AdminMapper adminMapper;

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<Admin> getByConditions(Admin admin) {
        return adminMapper.findByConditions(admin);
    }

    /**
     * 事务：
     * 开启事务之后，在本次事务之内做的所有的操作都不会直接作用到数据库中；
     * 只有当commit的时候，这些修改才有效；
     * 如果程序报错了，回滚数据库，回滚到数据的最初状态，也就是本次事务之内所有的操作无效；
     * @param admins
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean addBatch(List<Admin> admins) {

        // 待新增的有4条
        // 实际新增的有3条

        int result = adminMapper.insetAdmins(admins);

        if (result != admins.size()) {
            throw new RuntimeException("批量新增失败！");
        }

        return true;
    }

    @Override
    public RespBean login(String username, String password, HttpServletRequest request) {

        /**
         * 1 根据用户名查询用户对象
         * 2 验证密码：客户端传来的登录密码password 从数据库中查询的密码
         * 3 使用PasswordEncoder进行验证，验证通过 将用户信息写到session中
         */
        Admin adminFormDb = adminMapper.selectOne(new QueryWrapper<Admin>().eq("username", username));

        if (adminFormDb == null) {
            throw new YebException(501, "用户名或者密码不对！");
        }
        boolean result = false;
        try {
            result = passwordEncoder.verifyPwd(password, adminFormDb.getPassword());
        } catch (NoSuchAlgorithmException e) {

            throw new YebException(502, "验证密码时错误！");
        }
        if (result) {

            // TODO 根据用户的id，查询这个用户对应的权限列表
            List<Menu> menus = menuMapper.findByAdminId(adminFormDb.getId());
            adminFormDb.setMenus(menus);
            securityContextHolder.setUserDetails(adminFormDb);

            if (securityContextHolder instanceof JwtBasedSecurityContextHolder) {
                String token = Jwts.builder()
                        .signWith(SignatureAlgorithm.HS256, "test")
                        .setSubject(adminFormDb.getUsername())
                        .setExpiration(new Date(System.currentTimeMillis() + 30 * 60 * 1000))
                        .compact();

                return RespBean.success("登录成功！", token);
            }
            return RespBean.success("登录成功!");
        }
        throw new YebException(501, "用户名或者密码不正确！");
    }
}
