package com.example.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.server.pojo.Employee;
import com.example.server.mapper.EmployeeMapper;
import com.example.server.service.IEmployeeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.server.vo.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements IEmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public PageBean<Employee> getListInPage(Integer currentPage, Integer size) {

        // total 本次查询，如果不分页的话，应该有多少条数据 count(1)
        Integer count = employeeMapper.selectCount(new QueryWrapper<Employee>());

        // totalPage `15 / 5 = 3 公式：Math.ceil( total / size ) = totalPage
        // 16 / 5 = 3.x -> 4 向上取整
        // 16 / 5 = 3
        // 1L 1.1f 1.1 (doule)
        int totalPage = (int) Math.ceil(count * 1.0 / size);

        // rows 本页该查询的数据
        // select * from xxx limit ?,?
        List<Employee> rows = employeeMapper.findInPage(
                (currentPage - 1) * size,
                size
        );

        return new PageBean<Employee>()
                .setTotal(count)
                .setTotalPage(totalPage)
                .setRows(rows)
                .setCurrentPage(currentPage)
                .setSize(size);
    }

    @Override
    public PageBean<Employee> getListInPage2(Integer currentPage, Integer size) {

        // 插件的规则
        // 1. 封住分页的数据
        Page<Employee> employeePage = new Page<>(currentPage, size);

        // 2. 设计mapper接口，接口中第一个参数一定是Page对象
        IPage<Employee> result = employeeMapper.findInPage2(employeePage);

        return new PageBean<Employee>()
                .setTotal((int) result.getTotal())
                .setTotalPage((int) result.getPages())
                .setRows(result.getRecords())
                .setCurrentPage(currentPage)
                .setSize(size);
    }
}
