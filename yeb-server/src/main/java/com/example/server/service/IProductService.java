package com.example.server.service;

import com.example.server.pojo.Product;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author 小红
 * @since 2022-12-03
 */
public interface IProductService extends IService<Product> {

}
