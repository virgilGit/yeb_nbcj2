package com.example.server.security;

/**
 * 上下文对象，用来维护某个请求内部的、私有的、排他的数据
 * 这个数据仅仅在本次请求内部有效
 */
public interface SecurityContextHolder {

    UserDetails getUserDetails();

    void setUserDetails(UserDetails userDetails);
}
