package com.example.server.security;

public class JwtBasedSecurityContextHolder implements SecurityContextHolder {

    /**
     * ThreadLocal 线程的本地空间
     * 为每一个线程开辟一块独立的空间，这个空间是个排他空间，别的线程看不到另一个线程的空间；
     * Map<Thread, UserDetails>
     *     put(Thread.currentThread(), userDetails)
     *     get(Thread.currentThread())
     * Tomcat 服务器
     * 接受请求，返回响应；
     * 每一次服务器接受一个请求，单独开辟一个线程来运行这个请求，直到响应结束了，这个线程才会被回收；
     * 一个请求 对应了 一个线程
     * 因此ThreadLocal就可以为每一次请求保存独立的、排他的、临时的数据
     */
    private final ThreadLocal<UserDetails> AUTHENTICATION_CONTEXT = new ThreadLocal<>();

    @Override
    public UserDetails getUserDetails() {

        return AUTHENTICATION_CONTEXT.get();
    }

    @Override
    public void setUserDetails(UserDetails userDetails) {

        AUTHENTICATION_CONTEXT.set(userDetails);
    }
}
