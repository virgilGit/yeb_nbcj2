package com.example.server.security;

import com.example.server.pojo.Admin;

public interface UserDetailsService {

    UserDetails loadUserByUsername(String username);
}
