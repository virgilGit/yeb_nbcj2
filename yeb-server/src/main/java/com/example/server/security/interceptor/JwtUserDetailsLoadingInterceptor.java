package com.example.server.security.interceptor;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.server.mapper.AdminMapper;
import com.example.server.mapper.MenuMapper;
import com.example.server.pojo.Admin;
import com.example.server.pojo.Menu;
import com.example.server.security.SecurityContextHolder;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Component
public class JwtUserDetailsLoadingInterceptor implements HandlerInterceptor {

    /**
     * 进行登录
     */
    @Autowired
    private AdminMapper adminMapper;

    @Autowired
    private MenuMapper menuMapper;

    /**
     * 记录个人信息
     */
    @Autowired
    private SecurityContextHolder securityContextHolder;

    private final String AUTHORIZATION_PREFIX = "Bearer";

    private final String AUTHORIZATION_KEY = "Authorization";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 读取token
        // 从请求头中读取Authorization 也就是jwt令牌
        String authorization = request.getHeader(AUTHORIZATION_KEY);

        // 解析token
        // Bearer 消费型令牌 使用这个jwt就可以换个人信息
        // Refresh 刷新令牌
        // ....
        // Authorization : Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJ4eHh4eHh4eCIsInN1YiI6IlNub3ciLCJpYXQiOjE2NzA3MjYyMzYsInJvbGVzIjoiYWRtaW4sbWFuYWdlciIsImV4cCI6MTY3MDcyNjIzN30.-iAq1KtXGqqVgC5wruc5gIActJo4nUyNhGGqdln991c
        if (!StringUtils.isEmpty(authorization) && authorization.length() > AUTHORIZATION_PREFIX.length()) {

            String token = authorization.substring(AUTHORIZATION_PREFIX.length());
            Claims claims = Jwts.parser()
                    .setSigningKey("test")
                    .parseClaimsJws(token)
                    .getBody();

            String username = claims.getSubject();
            // 使用解析得到的个人信息，进行登录操作
            Admin admin = adminMapper.selectOne(new QueryWrapper<Admin>().eq("username", username));
            List<Menu> menus = menuMapper.findByAdminId(admin.getId());
            admin.setMenus(menus);

            securityContextHolder.setUserDetails(admin);
        }
        // 这个拦截器不做拦截，只是加载个人信息
        return true;
    }
}
