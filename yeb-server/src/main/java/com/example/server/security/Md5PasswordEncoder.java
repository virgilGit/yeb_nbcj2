package com.example.server.security;

import org.apache.tomcat.util.codec.binary.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5PasswordEncoder implements PasswordEncoder {

    @Override
    public String encodePwd(String password) throws NoSuchAlgorithmException {

        MessageDigest md5 = MessageDigest.getInstance("MD5");

        md5.update(password.getBytes());

        return Base64.encodeBase64String(md5.digest());
    }

    @Override
    public boolean verifyPwd(String password, String encoded) throws NoSuchAlgorithmException {

        // 将密码再加密下和密文做比较
        String newEncoded = encodePwd(password);

        return newEncoded.equals(encoded);
    }

}
