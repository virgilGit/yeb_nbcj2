package com.example.server.security;

import java.security.NoSuchAlgorithmException;

public interface PasswordEncoder {

    /**
     * 将密码加密为密文
     */
    String encodePwd(String password) throws NoSuchAlgorithmException;

    /**
     * 验证密码和密文是否匹配
     */
    boolean verifyPwd(String password, String encoded) throws NoSuchAlgorithmException;
}
