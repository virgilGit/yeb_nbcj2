package com.example.server.security;

public interface UserDetails {

    String getUsername();

    String getPassword();
}
