package com.example.server.security.interceptor;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.server.mapper.MenuMapper;
import com.example.server.pojo.Admin;
import com.example.server.pojo.Menu;
import com.example.server.security.SecurityContextHolder;
import com.example.server.security.UserDetails;
import com.example.server.security.YebException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AuthorizationInterceptor implements HandlerInterceptor {

    @Autowired
    private MenuMapper menuMapper;

    @Autowired
    private SecurityContextHolder securityContextHolder;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        /**
         * 1 获取请求的地址
         * 2 根据该地址匹配其对应的menu权限
         *
         * 3 将用户拥有的和本次请求需要的权限进行比较，如果存在交集
         * 就可以访问，否则拒绝访问
         *
         * request -> 验证有没有登录 AuthenticationInterceptor ->
         * 验证有没有资格访问当前请求 AuthorizationInterceptor --> aop -> controller
         */

        // 因为该地址是个详细的地址 /test/1
        // 但是数据库中保存的menu地址 /test/*
        // AntPathMatcher
        // 先把所有的menu都查询出来

        String requestURI = request.getRequestURI();

        List<Menu> menus = menuMapper.selectList(new QueryWrapper<Menu>());

        AntPathMatcher antPathMatcher = new AntPathMatcher();

        /**
         * for 单线程的循环 只能利用的一个cpu资源
         * 为了能够充分利用pc里的多个cpu核心，可以使用stream流
         * stream流会将循环的数据切成好几份，这几份分别由不同的cpu进行计算
         * 最后再将结果收集到一块就是最后的结果；
         * ----> mapReduce 大数据
         */
        List<Menu> collect = menus.stream() // 首先开启stream流 高级的for循环
                .filter(menu -> {
                    return antPathMatcher.match(menu.getUrl(), requestURI);
                }).collect(Collectors.toList());

        if (collect.size() == 0) {
            // 数据库中没有登记该资源，这里直接放行
            return true;
        }

        Menu allowedMenu = collect.get(0);

        Admin userDetails = (Admin) securityContextHolder.getUserDetails();
        List<Menu> menusFromUser = userDetails.getMenus();

        if (menusFromUser.contains(allowedMenu)) {
            return true;
        }
        throw new YebException(401, "用户权限不具备，无法访问该资源。");
    }
}
