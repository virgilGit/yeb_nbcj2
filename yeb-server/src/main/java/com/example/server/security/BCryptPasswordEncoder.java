package com.example.server.security;

import org.mindrot.jbcrypt.BCrypt;

public class BCryptPasswordEncoder implements PasswordEncoder {

    @Override
    public String encodePwd(String password) {

        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    @Override
    public boolean verifyPwd(String password, String encoded) {

        return BCrypt.checkpw(password, encoded);
    }
}
