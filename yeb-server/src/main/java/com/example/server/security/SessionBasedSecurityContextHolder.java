package com.example.server.security;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionBasedSecurityContextHolder implements SecurityContextHolder {

    @Override
    public UserDetails getUserDetails() {

        ServletRequestAttributes requestAttributes =
                (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();

        HttpSession session = request.getSession();

        UserDetails auth = (UserDetails) session.getAttribute("auth");

        return auth;
    }

    @Override
    public void setUserDetails(UserDetails userDetails) {

        ServletRequestAttributes requestAttributes =
                (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();

        HttpSession session = request.getSession();

        session.setAttribute("auth", userDetails);
    }
}
