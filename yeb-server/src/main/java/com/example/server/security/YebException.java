package com.example.server.security;

import lombok.Data;

@Data
public class YebException extends RuntimeException {

    private Integer code;

    public YebException() {}

    public YebException(Integer code, String msg) {

        super(msg);

        this.code = code;
    }
}
