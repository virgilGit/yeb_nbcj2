package com.example.server.security.interceptor;

import com.example.server.security.SecurityContextHolder;
import com.example.server.security.UserDetails;
import com.example.server.security.YebException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class AuthenticationInterceptor implements HandlerInterceptor {

    @Autowired
    private SecurityContextHolder securityContextHolder;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        /**
         * 拦截指定的请求，先执行该方法中的代码，当这个方法返回true了，再去执行后续的请求
         * 相当于@Before
         */

        UserDetails userDetails = securityContextHolder.getUserDetails();

        if (userDetails == null) {
            throw new YebException(403, "用户未登录！");
        }

        return true;
    }
}
