package com.example.server.mapper;

import com.example.server.pojo.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author 小红
 * @since 2022-12-03
 */
public interface ProductMapper extends BaseMapper<Product> {

}
