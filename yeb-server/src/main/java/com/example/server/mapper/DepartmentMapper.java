package com.example.server.mapper;

import com.example.server.pojo.Department;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
@Repository
public interface DepartmentMapper extends BaseMapper<Department> {

    Department findByIdWithEmps(@Param("id") Integer id);
}
