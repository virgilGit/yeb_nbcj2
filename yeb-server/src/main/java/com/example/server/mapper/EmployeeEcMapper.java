package com.example.server.mapper;

import com.example.server.pojo.EmployeeEc;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
public interface EmployeeEcMapper extends BaseMapper<EmployeeEc> {

}
