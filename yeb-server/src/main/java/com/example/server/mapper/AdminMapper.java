package com.example.server.mapper;

import com.example.server.pojo.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
@Repository
public interface AdminMapper extends BaseMapper<Admin> {

    List<Admin> findAllWithRoles();

    /**
     * 1 Java Bean实体类，这些值需要使用#{}来取出来
     * 需要在#{}中填写实体类的对应成员变量，就可以取出相应的值
     * @param admin
     * @return
     */
    List<Admin> findByConditions(Admin admin);

    /**
     * 2 使用@Param 注解来传值，注解中的value作为变量名在xml中使用#{}来获取
     * @param id
     * @return
     */
    @Select("select * from t_admin where id = #{id1}")
    Admin findById(@Param("id1") Integer id);

    /**
     * 使用@Param注解的时候：
     * 1 如果mapper方法有多个参数，需要使用@Param 进行区分
     * 2 @Param 修饰了JavaBean，取值的时候取的还是JavaBean的属性，都得添加@Param注解value值作为前缀
     * @param username
     * @param admin
     * @return
     */
    @Select("select * from t_admin where username = #{username} and password = #{admin.password}")
    List<Admin> findByUsernameAndPassword(@Param("username") String username,
                                          @Param("admin") Admin admin);

    /**
     * 3 使用map进行传值
     * #{}会通过map中的key来取出对应的值
     * @param params
     * @return
     */
    @Select("select * from t_admin where username = #{username} and password=#{password}")
    List<Admin> findByUsernameAndPassword2(Map<String, String> params);

    @Select("select count(1) from t_admin")
    Integer findAllCount();

    int insetAdmins(@Param("admins") List<Admin> admins);

    List<Admin> findAllWithRoles2();
}
