package com.example.server.mapper;

import com.example.server.pojo.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
@Repository
public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> findByParentId(@Param("parentId") Integer parentId);

    List<Menu> findByAdminId(@Param("adminId") Integer adminId);
}
