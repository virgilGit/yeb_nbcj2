package com.example.server.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.server.pojo.Employee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
@Repository
public interface EmployeeMapper extends BaseMapper<Employee> {

    List<Employee> findAllWithDept();

    @Select("select * from t_employee limit #{index}, #{size}")
    List<Employee> findInPage(@Param("index") int index, @Param("size") Integer size);

    @Select("select * from t_employee")
    IPage<Employee> findInPage2(Page<Employee> employeePage);
}
