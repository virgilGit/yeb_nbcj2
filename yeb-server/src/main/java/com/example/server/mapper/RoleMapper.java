package com.example.server.mapper;

import com.example.server.pojo.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小红
 * @since 2022-09-17
 */
public interface RoleMapper extends BaseMapper<Role> {

    List<Role> findByAdminId(@Param("adminId") Integer adminId);
}
