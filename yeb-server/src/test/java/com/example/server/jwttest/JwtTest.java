package com.example.server.jwttest;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

public class JwtTest {

    public static void main(String[] args) throws InterruptedException {

        // 构造一个jwt
        String token = Jwts.builder()
                .setId("xxxxxxxx") // jwt的唯一标识符
                .setSubject("Snow") // jwt是颁发给谁的
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "xxx".getBytes())
                .claim("roles", "admin,manager") // 自定义jwt内容
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * 1)) // 截止时间
                .compact();

        System.out.println(token);

        // 做数据加密的时候，基于传输层进行加密，SSL证书
        // https://
        Thread.sleep(2000);

        // 校验jwt
        Claims claims = Jwts.parser()
                .setSigningKey("xxx".getBytes())
                .parseClaimsJws(token)
                .getBody();

        System.out.println(claims);
    }
}
