package com.example.server.crypt;

import org.mindrot.jbcrypt.BCrypt;

public class BcryptTest {

    public static void main(String[] args) {

        String gensalt = BCrypt.gensalt();
        System.out.println(gensalt);
        String hashpw = BCrypt.hashpw("123456", "$2a$10$50/CXY4cmD5RWGTzlkV1o.");

        System.out.println(hashpw);

        boolean checkpw = BCrypt.checkpw("123456", hashpw);

        System.out.println(checkpw);

        // $2a$10$AUmbkTNx/FhbySD0MnFFHeZIR2HcDVLfiSs692o4Sj6exV.pnjCxK
        // $2a$10$oNIb2qc9D6RGO9gLklNLv.s12/pw8I/IyujxnvuYkCgAqnhjwW2We

        /**
         * $2a$10$50/CXY4cmD5RWGTzlkV1o.
         * $2a$10$50/CXY4cmD5RWGTzlkV1o.J1FVRx6nM5v7womz7FtFifSrML5YagW
         *
         * $2a$10$50/CXY4cmD5RWGTzlkV1o.J1FVRx6nM5v7womz7FtFifSrML5YagW
         *
         * BCrypt 先生成salt 使用这个salt生成密文
         * 将salt + 密文 == 结果
         *
         * 解析的时候，也是将密文结构拆开来，salt 密文 --> 原文
         */


    }
}
