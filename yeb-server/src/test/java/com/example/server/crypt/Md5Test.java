package com.example.server.crypt;

import org.apache.tomcat.util.codec.binary.Base64;

import java.security.MessageDigest;

public class Md5Test {

    public static void main(String[] args) throws Exception {

        // md5 不算是加密算法，属于散列算法
        // 原文 --> 密文
        // 不能再 密文 --> 原文

        // 123 --> xxxxxx
        // 登录的时候 客户端提供了密码123 数据库中保存了密码xxxxxx
        // 再验证的时候，将123 按照原先的算法 -> xxxxxx 比较两串密文是否一致

        MessageDigest md5 = MessageDigest.getInstance("MD5");

        md5.update("123456".getBytes());

        byte[] digest = md5.digest(); // 摘要

        String s = Base64.encodeBase64String(digest);

        System.out.println(s);
    }
}
