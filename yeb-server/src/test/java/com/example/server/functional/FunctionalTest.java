package com.example.server.functional;

import com.example.server.pojo.Admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FunctionalTest {

    /**
     * 制作一个筛选地功能
     * 筛选admin列表中id> 10 地所有地数据
     * 筛选admin中所有name以“==”开头地数据
     */
    public static <T> List<T> filterAdmin(List<T> resourceList, ConditionalMethod<T> conditionalMethod) {

        List<T> targetList = new ArrayList<>();

        for (T admin : resourceList) {

            if (conditionalMethod.test(admin)) {
                targetList.add(admin);
            }
        }

        return targetList;
    }

    public static void main(String[] args) {

        List<Admin> admins = Arrays.asList(
                new Admin().setId(1).setName("name1"),
                new Admin().setId(11).setName("name11"),
                new Admin().setId(2).setName("xname2"),
                new Admin().setId(21).setName("xname21")
        );

        admins.forEach((x) -> System.out.println(x));

        /**
         * 将一个方法（代码、功能、实现）作为一个参数（对象、基本数据类型）传递到另外一个方法中
         * 函数式编程
         */
        List<Admin> result = filterAdmin(admins, (admin) -> {
            return admin.getName().startsWith("x");
        });

        for (Admin admin : result) {
            System.out.println(admin);
        }
    }
}
