package com.example.server.functional;

import com.example.server.pojo.Admin;

@FunctionalInterface // 函数式接口
public interface ConditionalMethod<T> {

    boolean test(T admin);
}
