package com.example.server.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProxyTest {

    public static void main(String[] args) {

        Actor actor = new Actor();

        // 动态代理，构造一个代理对象
        /**
         * 1.5
         * 19
         * 匿名内部类
         * 接口是没法直接new一个对象的；必须实现这个接口，再使用实现类new一个对象；
         * 或者使用匿名内部类的方式，在编写代码的时候临时地去实现当前地接口，并且new一个对象；
         */
        IActor proxyActor = (IActor) Proxy.newProxyInstance(
                actor.getClass().getClassLoader(),
                actor.getClass().getInterfaces(),
                /**
                 * lambda 表达式 1.8
                 * 用来代替匿名内部类，只能够代替 函数式接口 地匿名内部类
                 * 函数式接口：只有一个抽象方法地接口称为函数式接口
                 */
                (proxy, method, objs) -> {
                    String name = method.getName();
                    Object result = null;
                    float money = (float) objs[0];

                    if ("basicAct".equals(name)) {

                        if (money > 1000) {
                            // 让演员正常出演
                            // Method对象 方法对象
                            // invoke 执行方法，需要指定是哪个对象执行力这个方法，参数有哪些；
                            result = method.invoke(actor, objs);
                        } else {
                            System.out.println("经纪人拒绝让演员出演普通表演！");
                        }
                    } else if ("dangerAct".equals(name)) {

                        if (money > 1500) {
                            result = method.invoke(actor, objs);
                        } else {
                            System.out.println("经纪人拒绝让演员出演危险表演！");
                        }
                    }
                    return result;
                }
        );

        actor.basicAct(1000);
        actor.dangerAct(2000);

        proxyActor.basicAct(1000);
        proxyActor.dangerAct(2000);
    }
}
